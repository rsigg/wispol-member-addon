<?php
/**
 * Load API functions.
 *
 * @author      iThemes
 * @since       1.0
 * @copyright   2015 (c) Iron Bound Designs, 2016 iThemes.
 * @license     GPLv2
 */

require_once( \ITEGMS\Plugin::$dir . 'api/members.php' );
require_once( \ITEGMS\Plugin::$dir . 'api/purchases.php' );